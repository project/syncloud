<?php

namespace Drupal\syncloud\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a syn entity type.
 */
interface SynInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the syn name.
   *
   * @return string
   *   Name of the syn.
   */
  public function getName();

  /**
   * Sets the syn name.
   *
   * @param string $name
   *   The syn name.
   *
   * @return \Drupal\syncloud\SynInterface
   *   The called syn entity.
   */
  public function setName($name);

  /**
   * Gets the syn name.
   *
   * @return string
   *   Name of the syn.
   */
  public function getHost();

  /**
   * Sets the syn name.
   *
   * @param string $host
   *   The syn name.
   *
   * @return \Drupal\syncloud\SynInterface
   *   The called syn entity.
   */
  public function setHost($host);

  /**
   * Gets the syn name.
   *
   * @return string
   *   Name of the syn.
   */
  public function getScheme();

  /**
   * Sets the syn name.
   *
   * @param string $scheme
   *   The syn name.
   *
   * @return \Drupal\syncloud\SynInterface
   *   The called syn entity.
   */
  public function setScheme($scheme);

  /**
   * Gets the syn creation timestamp.
   *
   * @return int
   *   Creation timestamp of the syn.
   */
  public function getCreatedTime();

  /**
   * Sets the syn creation timestamp.
   *
   * @param int $timestamp
   *   The syn creation timestamp.
   *
   * @return \Drupal\syncloud\SynInterface
   *   The called syn entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the syn status.
   *
   * @return bool
   *   TRUE if the syn is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the syn status.
   *
   * @param bool $status
   *   TRUE to enable this syn, FALSE to disable.
   *
   * @return \Drupal\syncloud\SynInterface
   *   The called syn entity.
   */
  public function setStatus($status);

}
