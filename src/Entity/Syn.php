<?php

namespace Drupal\syncloud\Entity;

use Drupal\syncloud\Utility\ContentEntity;
use Drupal\syncloud\Utility\FieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the syn entity class.
 *
 * @ContentEntityType(
 *   id = "syn",
 *   label = @Translation("Syn"),
 *   label_collection = @Translation("Syn Data"),
 *   handlers = {
 *     "view_builder" = "Drupal\syncloud\Entity\SynViewBuilder",
 *     "list_builder" = "Drupal\syncloud\Entity\SynListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\syncloud\Entity\SynAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\syncloud\Form\SynForm",
 *       "edit" = "Drupal\syncloud\Form\SynForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "syn",
 *   admin_permission = "administer syn",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/synapse/syn/add",
 *     "canonical" = "/admin/synapse/syn/{syn}",
 *     "edit-form" = "/admin/synapse/syn/{syn}/edit",
 *     "delete-form" = "/admin/synapse/syn/{syn}/delete",
 *     "collection" = "/admin/content/syn"
 *   },
 *   field_ui_base_route = "entity.syn.settings"
 * )
 */
class Syn extends ContentEntity implements SynInterface {

  /**
   * Default Value Callback.
   */
  public static function matomoCallback() {
    $matomo = FALSE;
    $cookies = \Drupal::request()->cookies;
    foreach ($cookies->keys() as $key) {
      if (substr($key, 0, 6) == '_pk_id') {
        $val = $cookies->get($key);
        $matomo = strstr($val, '.', TRUE);
      }
    }
    return $matomo;
  }

  /**
   * Default Value Callback.
   */
  public static function extraCallback() {
    $extra['google'] = \Drupal::request()->cookies->get('_ga');
    $extra['yandex'] = \Drupal::request()->cookies->get('_ym_uid');
    $extra['calltouch'] = \Drupal::request()->cookies->get('_ct_session_id');
    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['name'] = FieldDefinition::string('Name');
    $fields['mode'] = FieldDefinition::string('Mode');
    $fields['host'] = FieldDefinition::string('Host');
    $fields['scheme'] = FieldDefinition::string('Scheme');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    $self = "Drupal\syncloud\Entity\Syn";
    $fields['syn_id'] = FieldDefinition::string('Syn ID');
    $fields['syn_id']->setDefaultValueCallback("{$self}::matomoCallback");
    $fields['type'] = FieldDefinition::string('Type');
    $fields['message'] = FieldDefinition::entity("Contact Message", 'contact_message');
    $fields['ip'] = FieldDefinition::string('IP');
    // @todo = колбек для айпишника.
    $fields['city'] = FieldDefinition::string('City');
    $fields['url'] = FieldDefinition::string('Url');
    $fields['extra'] = FieldDefinition::map('Extra');
    $fields['extra']->setDefaultValueCallback("{$self}::extraCallback");
    return $fields;
  }

}
