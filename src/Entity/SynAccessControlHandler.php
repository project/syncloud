<?php

namespace Drupal\syncloud\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the syn entity type.
 */
class SynAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view syn');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
          $account, ['edit syn', 'administer syn'], 'OR'
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
          $account, ['delete syn', 'administer syn'], 'OR'
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account, ['create syn', 'administer syn'], 'OR'
    );
  }

}
