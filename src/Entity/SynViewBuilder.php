<?php

namespace Drupal\syncloud\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaBot entity type.
 */
class SynViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $build['hello'] = [
        '#markup' => "world",
      ];
    }
    return $build;
  }

}
