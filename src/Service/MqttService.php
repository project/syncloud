<?php

namespace Drupal\syncloud\Service;

use Bluerhinos\phpMQTT;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Mqtt Service service.
 */
class MqttService {

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a MqttService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('syncloud.settings');
  }

  /**
   * RabbitMQTT init.
   */
  public function getMqttSettings() {
    return [
      'server' => $this->config->get('server'),
      'port' => $this->config->get('port'),
      'username' => $this->config->get('login'),
      'password' => $this->config->get('password'),
    ];
  }

  /**
   * RabbitMQTT init.
   */
  public function run(array $mqtt_settings = []) {
    if (empty($mqtt_settings)) {
      $mqtt_settings = $this->getMqttSettings();
    }
    $client_id = 'phpMQTT-client' . rand();
    $mqtt = @(new phpMQTT($mqtt_settings['server'], $mqtt_settings['port'], $client_id));
    if ($mqtt->connect(TRUE, NULL, $mqtt_settings['username'], $mqtt_settings['password'])) {
      $this->mqtt = $mqtt;
    }
    else {
      $this->mqtt = FALSE;
      \Drupal::messenger()->addError('MQTT ERR');
    }
    return $this;
  }

  /**
   * Cron Task.
   */
  public function getClient() {
    $mqtt = $this->run()->mqtt;
    return $mqtt;
  }

  /**
   * Cron Task.
   */
  public function cronTask($now) {
    $mqtt = $this->run()->mqtt;
    $mqtt->publish("com.biz-panel/mqtt/cron", "Cron at $now");
    $mqtt->close();
  }

  /**
   * RabbitMQTT Publish.
   */
  public function publish($topic, $message) {
    $qos = 0;
    $retain = FALSE;
    if ($this->mqtt) {
      $this->mqtt->publish($topic, "$message", $qos, $retain);
      $this->mqtt->close();
    }
    return $this;
  }

  /**
   * RabbitMQTT Subscribe.
   */
  public function getMessage($topic) {
    $msg = $this->mqtt->subscribeAndWaitForMessage($topic, 0);
    $this->mqtt->close();
  }

  /**
   * RabbitMQTT Subscribe.
   */
  public function subscribe($topics = []) {
    $topics['com.biz-panel/mqtt'] = [
      'qos' => 0,
      'function' => 'procMsg',
    ];
    if ($this->mqtt) {
      $this->mqtt->subscribe($topics, 0);
      while ($this->mqtt->proc()) {
        // Do somthing.
      }
      $this->mqtt->close();
    }
    return $this;
  }

  /**
   * Message Callback.
   */
  public static function procMsg($topic, $msg) {
    $result = "Msg: [$topic]: $msg";
    \Drupal::messenger()->addError($result);
  }

}
