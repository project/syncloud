<?php

namespace Drupal\syncloud\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

/**
 * Class Connector.
 */
class Queue {

  /**
   * Init funciton.
   */
  public function __construct() {
  }

  /**
   * Hook.
   */
  public function guzzlePushQueue() {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $client = new Client([
      'base_uri' => $host,
      'timeout' => 1,
    ]);
    try {
      $response = $client->get("$host/syncloud/queue");
      $message = $response->getStatusCode();
    }
    catch (ConnectException $e) {
      $message = 'ConnectException ' . $e->getMessage();
    }
    catch (RequestException $e) {
      $message = 'RequestException ' . $e->getMessage();
    }
    catch (ClientException $e) {
      $message = 'ClientException ' . $e->getMessage();
    }
    catch (GuzzleException $e) {
      $message = 'GuzzleException ' . $e->getMessage();
    }
    return $message;
  }

  /**
   * Push entity to queue.
   */
  public function queuePush(int $id, $syn_id) {
    $queue = \Drupal::queue('syncloud_queue');
    $queue->createQueue();
    $queue->createItem([
      'id' => $id,
      'syn_id' => $syn_id,
    ]);
  }

  /**
   * Process queue.
   */
  public function queueProcess() {
    $end = time() + 30;
    $queue = \Drupal::queue('syncloud_queue');
    while (time() < $end && ($item = $queue->claimItem())) {
      $syn = \Drupal::entityTypeManager()->getStorage('syn')->load($item->data['syn_id']);
      $storage = \Drupal::entityTypeManager()->getStorage($syn->type->value);
      $entity = $storage->load($item->data['id']);
      $config = \Drupal::configFactory()->get('syncloud.settings');
      if ($config->get('telegram-int')) {
        $uuid = \Drupal::state()->get('syncloud.uuid');
        $extra = $syn->extra->getValue()[0];
        $msg = [
          'ip' => $syn->ip->value,
          'type' => $syn->type->value,
          'host' => $syn->host->value,
          'mode' => $syn->mode->value,
          'matomo' => $syn->syn_id->value,
          'url' => $syn->url->value,
          'yandex' => $extra['yandex'],
          'google' => $extra['google'],
        ];
        if ($msg['type'] == 'commerce_order') {
          // Корзина закомплитилась.
          /** @var \Drupal\commerce_order\Entity\OrderInterface $entity */
          $profile_data = $entity->getBillingProfile() ? $entity->getBillingProfile()->toArray() : [];
          foreach ($profile_data as $key => $value) {
            if (strpos($key, "field_customer_") !== FALSE && isset($value[0]['value'])) {
              $msg[$profile_data['uid'][0]['target_id']][str_replace("field_customer_", "commerce--field_", $key)] = $value[0]['value'];
            }
          }

          // Элементы заказа.
          foreach ($entity->getItems() ?? [] as $order_item) {
            $currency = $order_item->getTotalPrice()->getCurrencyCode();
            $title = $order_item->getTitle();
            $msg[$title]['commerce--field_title'] = $order_item->getTitle();
            $msg[$title]['commerce--field_price'] = round($order_item->getTotalPrice()->getNumber(), 2) . " $currency";
            $msg[$title]['commerce--field_quantity'] = round($order_item->getQuantity(), 0);
            $msg[$title]['commerce--field_adjusted_price'] = round($order_item->getAdjustedTotalPrice()->getNumber(), 2) . " $currency";
          }

          // Сам заказ.
          $order_data = $entity->toArray();
          $msg['order']['commerce--field_order_number'] = $order_data['order_number'][0]['value'];
          if (isset($order_data['shipments']) && !empty($order_data['shipments'][0])) {
            $shipment_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipment');
            $shipment = $shipment_storage->load($order_data['shipments'][0]['target_id']);
            $msg['total']['commerce--field_shipping_method'] = $shipment ? $shipment->getShippingMethod()->getName() : '';
          }
          if ($entity->hasField('payment_gateway') && $gateway = $entity->payment_gateway->entity) {
            $conf = $gateway->getPluginConfiguration();
            if ($gateway->id() !== 'cash') {
              $msg['total']['commerce--field_payment_gateway'] = "{$conf['display_label']}: {$conf['gateway']}";
            }
            else {
              $msg['total']['commerce--field_payment_gateway'] = $conf['display_label'];
            }
            $msg['total']['commerce--field_total_price'] = round($entity->getTotalPrice()->getNumber(), 2) . " $currency";
          }
          \Drupal::moduleHandler()->alter('syncloud_queue_preprocess_commerce', $entity, $syn->syn_id->value);
        }
        elseif ($entity->getEntityTypeId() == 'webform_submission') {
          $msg['type'] = 'webform_submission';

          // Вебформа.
          foreach ($entity->getData() ?? [] as $key => $value) {
            $msg["message--field_$key"] = $value;
          }
          $msg["message--field_webform_id"] = $entity->bundle();
          $msg["message--field_webform_title"] = $entity->getWebform()->get('title');
          \Drupal::moduleHandler()->alter('syncloud_queue_preprocess_webform', $entity, $syn->syn_id->value);
        }
        else {

          // Контактная форма.
          foreach ($entity->getFields() ?? [] as $name => $field) {
            $msg['message--' . $name] = $field->getString();
          }
          \Drupal::moduleHandler()->alter('syncloud_queue_preprocess_contactform', $entity, $syn->syn_id->value);
        }
        \Drupal::service('syncloud.mqtt')->run()->publish("\$telega/syncloud/$uuid/event/contact-message", json_encode($msg));
      }
      $queue->deleteItem($item);
    }
  }

}
