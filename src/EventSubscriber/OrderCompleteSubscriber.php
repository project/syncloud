<?php

namespace Drupal\syncloud\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * This is OrderCompleteSubscriber.
 *
 * @package Drupal\syncloud
 */
class OrderCompleteSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];

    return $events;
  }

  /**
   * Method is called when commerce_order.place.post_transition is dispatched.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Event.
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $order_data = $order->toArray();
    $syn_storage = \Drupal::entityTypeManager()->getStorage('syn');
    $form_id = 'commerce_order';
    $ip = \Drupal::request()->getClientIp();
    $path = \Drupal::service('path.current')->getPath();

    $syn_array = [
      'name' => $ip . '--' . $form_id,
      'ip' => $ip,
      'host' => \Drupal::request()->getHost(),
      'scheme' => \Drupal::request()->getScheme(),
      'type' => $form_id,
      'message' => $order->id(),
      'mode' => \Drupal::request()->cookies->get('synhelper') ?? 'default',
      'city' => [],
      'url' => \Drupal::request()->getSchemeAndHttpHost() . \Drupal::service('path_alias.manager')->getAliasByPath($path),
    ];
    $syn = $syn_storage->create($syn_array);
    $syn_storage->save($syn);
    \Drupal::service('syncloud.queue')->queuePush($order->id(), $syn->id());
    \Drupal::service('syncloud.queue')->guzzlePushQueue();
  }

}
