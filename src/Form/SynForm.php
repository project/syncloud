<?php

namespace Drupal\syncloud\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the syn entity edit forms.
 */
class SynForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + [
      'link' => \Drupal::service('renderer')->render($link),
    ];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New syn %label has been created.', $message_arguments));
      $this->logger('syncloud')->notice('Created new syn %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The syn %label has been updated.', $message_arguments));
      $this->logger('syncloud')->notice('Updated new syn %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.syn.canonical', ['syn' => $entity->id()]);
  }

}
