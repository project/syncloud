<?php

namespace Drupal\syncloud\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for a syn entity type.
 */
class SynSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'syn_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('syncloud.settings');
    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
    ];
    $form['settings']['enable'] = [
      '#title' => $this->t('Enable SynCloud'),
      '#description' => $this->t('Add SynCloud code on website pages'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('enable'),
    ];
    $form['settings']['admin-pages'] = [
      '#title' => $this->t('Display SynCloud on admin pages (if SynCloud enabled)'),
      '#description' => $this->t('Add SynCloud code on admin pages'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('admin-pages'),
    ];
    $form['settings']['admin-disable'] = [
      '#title' => $this->t('Disable for admin'),
      '#description' => $this->t('Do not use SynCloud for User with uid=1'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('admin-disable'),
    ];
    $form['settings']['user-disable'] = [
      '#title' => $this->t('Disable for users'),
      '#description' => $this->t('Do not use SynCloud for all registered Users'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('user-disable'),
    ];
    $form['settings']['telegram-int'] = [
      '#title' => $this->t('Telegram integration'),
      '#description' => $this->t('Implement Telegram intagration'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('telegram-int'),
    ];
    $form['settings']['site-id'] = [
      '#title' => $this->t('Site ID'),
      '#default_value' => $config->get('site-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
      '#description' => $this->t("Some description"),
    ];
    $form['custom'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom'),
      '#open' => FALSE,
    ];
    $form['custom']['custom-matomo'] = [
      '#title' => $this->t('Custom matomo'),
      '#default_value' => $config->get('custom-matomo'),
      '#maxlength' => 255,
      '#size' => 55,
      '#type' => 'textfield',
      '#description' => $this->t("Path for not our hosting"),
    ];
    $form['cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron'),
      '#open' => FALSE,
    ];
    $form['cron']['weekly'] = [
      '#title' => $this->t('Weekly'),
      '#description' => $this->t('Send information via cron weekly'),
      '#type' => 'checkbox',
      '#maxlength' => 20,
      '#required' => FALSE,
      '#size' => 15,
      '#default_value' => $config->get('weekly'),
    ];
    $form['cron']['server'] = [
      '#title' => $this->t('Server'),
      '#description' => $this->t('mqtt server'),
      '#maxlength' => 255,
      '#size' => 55,
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $config->get('server'),
    ];
    $form['cron']['port'] = [
      '#title' => $this->t('Port'),
      '#description' => $this->t('mqtt server port'),
      '#maxlength' => 255,
      '#size' => 55,
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $config->get('port'),
    ];
    $form['cron']['login'] = [
      '#title' => $this->t('Login'),
      '#description' => $this->t('mqtt server login'),
      '#maxlength' => 255,
      '#size' => 55,
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $config->get('login'),
    ];
    $form['cron']['password'] = [
      '#title' => $this->t('Password'),
      '#description' => $this->t('mqtt server password'),
      '#maxlength' => 255,
      '#size' => 55,
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $config->get('password'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('syncloud.settings');
    $config
      ->set('enable', $form_state->getValue('enable'))
      ->set('admin-pages', $form_state->getValue('admin-pages'))
      ->set('admin-disable', $form_state->getValue('admin-disable'))
      ->set('user-disable', $form_state->getValue('user-disable'))
      ->set('telegram-int', $form_state->getValue('telegram-int'))
      ->set('site-id', $form_state->getValue('site-id'))
      ->set('custom-matomo', $form_state->getValue('custom-matomo'))
      ->set('weekly', $form_state->getValue('weekly'))
      ->set('server', $form_state->getValue('server'))
      ->set('port', $form_state->getValue('port'))
      ->set('login', $form_state->getValue('login'))
      ->set('password', $form_state->getValue('password'))
      ->save();
    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

}
