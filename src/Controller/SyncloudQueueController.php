<?php

namespace Drupal\syncloud\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller AppPage.
 */
class SyncloudQueueController extends ControllerBase {

  /**
   * Render Page.
   */
  public function page() {
    sleep(5);
    \Drupal::service('syncloud.queue')->queueProcess();
    return [];
  }

}
