<?php

namespace Drupal\syncloud\Hook;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Markup;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

/**
 * PreprocessHtml.
 */
class Requirements {

  /**
   * Hook.
   */
  public static function hook($phase) {
    $requirements = [];
    if ($phase === 'runtime') {
      $mqtt = \Drupal::service('syncloud.mqtt')->getMqttSettings();
      if ($mqtt['username'] === 'stat:public' || $mqtt['password'] === 'notSecured') {
        $host = 'https://app.biz-panel.com';
        $client = new Client([
          'base_uri' => "$host/syncloud-info/log_pass",
          'timeout'  => 1,
        ]);
        try {
          $uuid = \Drupal::state()->get('syncloud.uuid');
          $response = $client->get("$host/syncloud-info/log_pass/$uuid");
          $data = Json::decode($response->getBody()->getContents());
          $syncloud_settings = \Drupal::configFactory()->getEditable('syncloud.settings');
          $syncloud_settings->set('login', $data['login']);
          $syncloud_settings->set('password', $data['password']);
          $syncloud_settings->save();
          $message = $response->getStatusCode();
        }
        catch (ConnectException $e) {
          $message = 'ConnectException ' . $e->getMessage();
        }
        catch (RequestException $e) {
          $message = 'RequestException ' . $e->getMessage();
        }
        catch (ClientException $e) {
          $message = 'ClientException ' . $e->getMessage();
        }
        catch (GuzzleException $e) {
          $message = 'GuzzleException ' . $e->getMessage();
        }
        $msg = t('<span>Current Syncloud has public login or password</span>');
        $requirements = [
          'syncloud_login_pass' => [
            'title' => t('Syncloud: public login/pass'),
            'value' => Markup::create($msg),
            'severity' => REQUIREMENT_ERROR,
          ],
        ];
      }
    }
    return $requirements;
  }

}
