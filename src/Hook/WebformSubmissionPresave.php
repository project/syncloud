<?php

namespace Drupal\syncloud\Hook;

/**
 * @file
 * Contains \Drupal\syncloud\Hook.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Entity Insert.
 */
class WebformSubmissionPresave {

  /**
   * Hook.
   */
  public static function hook(EntityInterface $entity, $init = FALSE) {
    if ($entity->isCompleted() && $entity->id()) {
      $syn_storage = \Drupal::entityTypeManager()->getStorage('syn');
      $ip = \Drupal::request()->getClientIp();
      $path = \Drupal::service('path.current')->getPath();

      $syn_array = [
        'name' => $ip . '--' . $entity->getEntityType()->id(),
        'ip' => $ip,
        'host' => \Drupal::request()->getHost(),
        'scheme' => \Drupal::request()->getScheme(),
        'type' => $entity->getEntityType()->id(),
        'message' => $entity->id(),
        'mode' => \Drupal::request()->cookies->get('synhelper') ?? 'default',
        'city' => [],
        'url' => \Drupal::request()->getSchemeAndHttpHost() . \Drupal::service('path_alias.manager')->getAliasByPath($path),
      ];
      $syn = $syn_storage->create($syn_array);
      $syn_storage->save($syn);
      \Drupal::service('syncloud.queue')->queuePush($entity->id(), $syn->id());
      \Drupal::service('syncloud.queue')->guzzlePushQueue();
    }
  }

}
