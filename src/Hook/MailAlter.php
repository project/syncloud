<?php

namespace Drupal\syncloud\Hook;

use Drupal\Core\Render\Markup;

/**
 * @file
 * Contains \Drupal\syncloud\Hook.
 */

/**
 * Class PageAttachmentsAlter.
 */
class MailAlter {

  /**
   * Hook.
   */
  public static function hook(&$message, $config) {
    $matomo = self::matomoCallback();
    if (!empty($matomo)) {
      $message['body'][] = "\n" . Markup::create("<div>Полную историю посещений можно увидеть <a href='https://biz-panel.com/clickhouse/profile/$matomo'>здесь</a></div>");
    }
  }

  /**
   * Default Value Callback.
   */
  public static function matomoCallback() {
    $matomo = FALSE;
    $cookies = \Drupal::request()->cookies;
    foreach ($cookies->keys() as $key) {
      if (substr($key, 0, 6) == '_pk_id') {
        $val = $cookies->get($key);
        $matomo = strstr($val, '.', TRUE);
      }
    }
    return $matomo;
  }

}
