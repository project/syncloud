<?php

namespace Drupal\syncloud\Hook;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

/**
 * Hook Cron - send Emails.
 */
class Cron {

  /**
   * Hook.
   */
  public static function hook() {
    $config = \Drupal::configFactory()->get('syncloud.settings');
    $syncloud_weekly = \Drupal::state()->get('syncloud.weekly');
    $time = strtotime('now');
    if ($config->get('weekly') && $time - $syncloud_weekly > 604800) {
      self::checkMqtt();
      $uuid = \Drupal::state()->get('syncloud.uuid');
      $secret = \Drupal::state()->get('syncloud.secret');
      $url_options = [
        'absolute' => TRUE,
        'language' => \Drupal::languageManager()->getCurrentLanguage(),
      ];
      $site_url = Url::fromRoute('<front>', [], $url_options)->toString();
      $msg = [
        'url' => $site_url,
        'login' => $config->get('login'),
        'site' => $uuid,
        'secret' => $secret,
        'syncloud' => \Drupal::service('extension.list.module')->getList()['syncloud']->info['version'],
        'drupal' => \Drupal::VERSION,
        'php' => phpversion(),
      ];
      \Drupal::service('syncloud.mqtt')->run()->publish("stat/$uuid/state/usage", json_encode($msg));
      \Drupal::state()->set('syncloud.weekly', $time);
    }
  }

  /**
   * Check mqtt login/pass.
   */
  private static function checkMqtt() {
    $mqtt = \Drupal::service('syncloud.mqtt')->getMqttSettings();
    if ($mqtt['username'] === 'stat:public' || $mqtt['password'] === 'notSecured') {
      $host = 'https://app.biz-panel.com';
      $client = new Client([
        'base_uri' => "$host/syncloud-info/log_pass",
        'timeout'  => 1,
      ]);
      try {
        $uuid = \Drupal::state()->get('syncloud.uuid');
        $response = $client->get("$host/syncloud-info/log_pass/$uuid");
        $data = Json::decode($response->getBody()->getContents());
        $syncloud_settings = \Drupal::configFactory()->getEditable('syncloud.settings');
        $syncloud_settings->set('login', $data['login']);
        $syncloud_settings->set('password', $data['password']);
        $syncloud_settings->save();
        $message = $response->getStatusCode();
      }
      catch (ConnectException $e) {
        $message = 'ConnectException ' . $e->getMessage();
      }
      catch (RequestException $e) {
        $message = 'RequestException ' . $e->getMessage();
      }
      catch (ClientException $e) {
        $message = 'ClientException ' . $e->getMessage();
      }
      catch (GuzzleException $e) {
        $message = 'GuzzleException ' . $e->getMessage();
      }
      \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($message ?? [])]
      );

    }
  }

}
