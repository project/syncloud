<?php

namespace Drupal\syncloud\Hook;
/**
 * @file
 * Contains \Drupal\syncloud\Hook.
 */

use Drupal\Component\Utility\Crypt;

/**
 * Class PageAttachmentsAlter.
 */
class PageAttachments {

  /**
   * Hook.
   */
  public static function hook(&$page) {
    $config = \Drupal::config('syncloud.settings');
    $syncloud_uuid = \Drupal::state()->get('syncloud.uuid') ?: Crypt::randomBytesBase64(16);
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();
    $is_logged = \Drupal::currentUser()->isAuthenticated();
    $admin_pages = ($config->get('admin-pages') || !$is_admin);
    $users_enable = (!$config->get('user-disable') || !$is_logged);
    if (\Drupal::currentUser()->id() == 1 && $config->get('admin-disable')) {
      $admin_pages = FALSE;
    }
    if ($config->get('enable') && $admin_pages && $users_enable) {
      $site_id = $config->get('site-id') ?: 1;
      $host = $config->get('custom-matomo') ? $config->get('custom-matomo') . '/' : '/';
      $js = $host == "/" ? 's.js' : 'matomo.js';
      $html = $host == "/" ? 's.html' : 'matomo.php';
      $script = "
      <script type='text/javascript'>
        function getCookie(cname) {
          let name = cname + '=';
          let decodedCookie = decodeURIComponent(document.cookie);
          let ca = decodedCookie.split(';');
          let res = '';
          ca.forEach(function(elem) {
            let values = elem.split('=');
            let key = values[0].trim();
            if (key === cname) {
              res = values[1];
            }
          });
          return res;
        }
        let ga = getCookie('_ga');
        let ya = getCookie('_ym_uid');
        var _paq = _paq || [];
        var u = '$host';
        var d = document;
        g = d.createElement('script');
        s = d.getElementsByTagName('script')[0];
        _paq.push(['setDocumentTitle', d.domain + '/' + d.title]);
        _paq.push(['setTrackerUrl', u + '{$html}']);
        _paq.push(['setSiteId', '{$site_id}']);
        _paq.push(['setCustomDimension', 2, '$syncloud_uuid']);
        _paq.push(['setCustomDimension', 1, ya]);
        _paq.push(['setCustomDimension', 3, ga]);
        _paq.push(['enableLinkTracking']);
        _paq.push(['trackPageView']);
        g.type = 'text/javascript';
        g.async = true;
        g.defer = true;
        g.src = u + '{$js}';
        s.parentNode.insertBefore(g, s);
      </script>";
      $page['#attached']['html_head'][] = [
        ['#tag' => 'script', '#value' => $script],
        'syncloud',
      ];
    }
  }

}
