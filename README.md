# _Syncloud_

## Чем занимается?


## Требования к платформе

- _Drupal 9-11_
- _PHP 7.4.0+_

## Нужны модули

- [phpmqtt](https://github.com/politsin/phpmqtt)

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/syncloud)

```sh
composer require 'drupal/syncloud'
```

- [Drupal.org dev версия](https://www.drupal.org/project/syncloud/releases/8.x-1.x-dev)

```sh
composer require 'drupal/syncloud:1.x-dev@dev'
```

## Как использовать?

### Форма настроек

---

- Конфигурационная форма (Конфигурация > Система > syncloud или перейти по ссылке _/admin/structure/syn_)
  Форма содержит следующие поля:
  ### Настройки
  - [x] Enable Syncloud (использовать Syncloud)
  - [ ] Display Syncloud on admin pages (Использовать Syncloud на страницах админки)
  - [ ] Disable for admin (Отключить для администраторов)
  - [ ] Disable for anonymous users (Отключить для анонимных пользователей)
  - [x] Telegram integration (Интеграция с Telegram)
  - ID сайта
  ### Собственная
  - Custom matomo (URL не для наших сайтов)
  ### Cron
  - [x] Еженедельно (Еженедельная сихронизация)
  - Сервер (Url сервера)
  - Порт
  - Логин
  - Пароль
